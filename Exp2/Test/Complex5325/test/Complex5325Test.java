import junit.framework.TestCase;
import org.junit.Test;

public class Complex5325Test extends TestCase {
    Complex5325 sq = new Complex5325(1.0,4.0);
    Complex5325 gst = new Complex5325(1.0,3.0);
    @Test
    public void testAdd() {
        assertEquals("Complex{RealPart=2.0, ImagePart=7.0}", sq.ComplexAdd(gst).toString());
    }

    @Test
    public void testSub() {
        assertEquals("Complex{RealPart=0.0, ImagePart=1.0}", sq.ComplexSub(gst).toString());
    }

    @Test
    public void testMul() {
        assertEquals("Complex{RealPart=-11.0, ImagePart=7.0}", sq.ComplexMulti(gst).toString());
    }
}