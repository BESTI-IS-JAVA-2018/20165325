public class Complex5325 {
    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    double RealPart;
    double ImagePart;

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    public Complex5325(){}
    public Complex5325(double R,double I){
        this.ImagePart = I;
        this.RealPart = R;
    }


    @Override
    public String toString() {
        return "Complex{" +
                "RealPart=" + RealPart +
                ", ImagePart=" + ImagePart +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Complex5325 complex = (Complex5325) o;
        return Double.compare(complex.getRealPart(), getRealPart()) == 0 &&
                Double.compare(complex.getImagePart(), getImagePart()) == 0;
    }

    Complex5325 ComplexAdd(Complex5325 a) {
        return new Complex5325(this.RealPart + a.RealPart,this.ImagePart + a.ImagePart);
    }
    Complex5325 ComplexSub(Complex5325 a){
        return new Complex5325(this.RealPart - a.RealPart,this.ImagePart - a.ImagePart);
    }
    Complex5325 ComplexMulti(Complex5325 a){
        return new Complex5325(this.RealPart*a.RealPart-this.ImagePart*a.ImagePart,
                this.ImagePart*a.RealPart+this.RealPart*a.ImagePart);
    }
    Complex5325 ComplexDiv(Complex5325 a){
        double scale = a.getRealPart()*a.getRealPart() + a.getImagePart()*a.getImagePart();
        Complex5325 b = new Complex5325(a.getRealPart() / scale, - a.getImagePart() / scale);
        return this.ComplexMulti(b);
    }
}

