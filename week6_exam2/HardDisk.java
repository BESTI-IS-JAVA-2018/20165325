import java.util.Objects;

public class HardDisk {
    HardDisk(){

    }
    HardDisk (int m) {
        this.amount = m;
    }
    public int getAmount() {
        return amount;
    }

    public void setAmount(int m) {
        this.amount = m;
    }

    int amount;

    @Override
    public String toString() {
        return "HardDisk{" +
                "amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HardDisk hardDisk = (HardDisk) o;
        return getAmount() == hardDisk.getAmount();
    }

}
