import java.util.Random;
import java.util.Scanner;
public class Exp1 {
    public static void main(String[] args) {
        Random rand = new Random();
        int a = rand.nextInt(99) + 1;
        System.out.println("已经产生一个1-100的随机数a，请猜猜看它是多少？");
        Scanner g = new Scanner(System.in);
        int b = 0;
        do{
            try {
                b = g.nextInt();
                while (b > 100 || b < 1) {
                    System.out.println("请检查你的输入！再试一次，确保它在1-100之间：");
                    b = g.nextInt();
                }
            }
            catch (Exception e) {
                System.out.println("输入发生异常：" + e.getMessage());
                System.out.println("请检查你的输入！再试一次，确保它在1-100之间：");
                g = new Scanner(System.in);
                continue;
            }
            if (b>a) {
                System.out.println("不对，太大了！");
            }
            if (b<a) {
                System.out.println("不对，太小了！");
            }
        }while (a != b);
        System.out.println("猜对了！2333333333333333333333333\na = "+a);
    }
}
