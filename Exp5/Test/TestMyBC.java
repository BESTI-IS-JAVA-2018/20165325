import junit.framework.TestCase;
import org.junit.Test;

public class TestMyBC extends TestCase {
    @Test
    public void testTran(){
        assertEquals(MyBC.tran(new StringBuffer("1*2+(3-4/5)*6")).toString(),"1 2 * 3 4 5 / - 6 * +");
    }
}
