import javax.crypto.Cipher;
import java.io.*;
import java.net.Socket;
import java.security.Key;
import java.util.Scanner;

public class Client_2 {
    public static void main(String args[]) {
        Socket mysocket;
        DataInputStream in = null;
        DataOutputStream out = null;
        Scanner scanner = new Scanner(System.in);
        String str;
        try {
            FileInputStream f = new FileInputStream("key1.txt");
            ObjectInputStream b = new ObjectInputStream(f);
            Key k = (Key) b.readObject();
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.ENCRYPT_MODE, k);
            mysocket = new Socket("192.168.1.125", 3001);
            in = new DataInputStream(mysocket.getInputStream());
            out = new DataOutputStream(mysocket.getOutputStream());
            System.out.println("已连接服务器，请输入中缀表达式：");
            str = scanner.nextLine();
            byte ptext[] = MyBC.tran(new StringBuffer(str)).toString().getBytes("UTF-8");
            byte ctext[] = cp.doFinal(ptext);
            System.out.println("后缀表达式已被加密：");
            for (int i = 0; i < ctext.length; i++) {
                System.out.print(ctext[i] + ",");
            }
            System.out.println("");
            out.writeUTF(ctext.length + "");
            for (int i = 0; i < ctext.length; i++) {
                out.writeUTF(ctext[i] + "");
                //System.out.print();
            }
            String s = in.readUTF();   //in读取信息，堵塞状态
            System.out.println("客户收到服务器的回应:" + s);
        } catch (Exception e) {
            System.out.println("服务器已断开" + e);
        }
    }
}
