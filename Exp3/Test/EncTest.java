import junit.framework.TestCase;
import org.junit.Test;

public class EncTest extends TestCase {
    StringBuffer q = new StringBuffer("TORA!TORA!TORA!");
    StringBuffer w = new StringBuffer("WRUD!WRUD!WRUD!");

    @Test
    public void testEnc() {
        assertEquals(true, Enc.enc(3, q).toString().equals("WRUD!WRUD!WRUD!"));
    }

    @Test
    public void testDec() {
        assertEquals(true, Dec.dec(3, w).toString().equals("TORA!TORA!TORA!"));
    }
}
