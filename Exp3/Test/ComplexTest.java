import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex a = new Complex(5.0, 6.0);
    Complex b = new Complex(-3.0, 4.0);

    @Test
    public void testComplexAdd() {
        assertEquals(new Complex(2.0, 10.0), a.ComplexAdd(b));
    }

    @Test
    public void testComplexSub() {
        assertEquals(new Complex(8.0, 2.0), a.ComplexSub(b));
    }

    @Test
    public void testComplexMulti() {
        assertEquals(new Complex(-39.0, 2.0), a.ComplexMulti(b));
    }
}
