public class Dec {
    static StringBuffer dec(int key, StringBuffer s) {
        StringBuffer s0 = new StringBuffer(s);
        StringBuffer s1 = new StringBuffer();
        int i;
        char c;
        for (i = 0; i < s0.length(); i++) {
            if (('a' <= s0.charAt(i)) && (s0.charAt(i) <= 'z')) {
                c = (char) (s0.charAt(i) - key);
                if (c < 'a') {
                    c += 26;
                }
                if (c > 'z') {
                    c -= 26;
                }
                s1.append(c);
            } else if (('A' <= s0.charAt(i)) && (s0.charAt(i) <= 'Z')) {
                c = (char) (s0.charAt(i) - key);
                if (c < 'A') {
                    c += 26;
                }
                if (c > 'Z') {
                    c -= 26;
                }
                s1.append(c);
            } else {
                s1.append(s0.charAt(i));
            }
        }
        return s1;
    }
}
