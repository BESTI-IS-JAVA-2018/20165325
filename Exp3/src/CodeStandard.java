/**
 * Demo class
 *
 * @author lidongjun 20165325
 * @date 2018/04/24
 */
public class CodeStandard {
    public static void main(String[] args) {
        StringBuffer buffer = new StringBuffer();
        int m = 20;
        buffer.append('S');
        buffer.append("tringBuffer");
        System.out.println(buffer.charAt(1));
        System.out.println(buffer.capacity());
        System.out.println(buffer.indexOf("tring"));
        System.out.println("buffer = " + buffer.toString());
        if (buffer.capacity() < m) {
            buffer.append("1234567");
        }
        for (int i = 0; i < buffer.length(); i++) {
            System.out.println(buffer.charAt(i));
        }
    }
}