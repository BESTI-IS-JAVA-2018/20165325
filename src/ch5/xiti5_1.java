class Father {
	private int money=12;
	float height;
	int seeMoney(){
		return money;    //A
	}
	void reviseMoney(int a) {
		money = a;
	}
}


class Son extends Father {
	int height;
	int lookMoney() {
		int m = seeMoney();      //B
		return m;
	}
	void reviseHeight (float a) {
		super.height = a;
	}
}

	
class xiti5_1{
	public static void main(String args[]){
		Son erzi = new Son();
		erzi.reviseMoney(300); //C
		erzi.reviseHeight(1.78F);//B
	}
}
