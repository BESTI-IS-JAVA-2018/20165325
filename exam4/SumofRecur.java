public class SumofRecur {
     public static void main(String args[]) {
		int [] tmp = new int [args.length];
		for(int i=0; i<args.length; i++) {
            tmp[i] = Integer.parseInt(args[i]);
         }
		int sum = 0;
		if (tmp[0]<=0) {
			System.out.println("Please check your input!");
			System.exit(0);
		}
		for (int i=1;i<=tmp[0];i++) {
			sum += fact(i);
		}
		if (sum<=0) {
			System.out.println("Please check your input!"); 
			System.exit(0);
		}
		System.out.println(sum);
    }
    public static int fact(int n) {
        if (n == 0)
             return 1;
         else
             return n * fact(n-1);
    }
}
