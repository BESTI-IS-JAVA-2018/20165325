public class Mystack {
    int top = -1;
    String stack [] = new String[100];

    Mystack(){};
    public Mystack(StringBuffer s) {
        int i = 0;
        StringBuffer q = new StringBuffer();
        for (i = 0; i<s.toString().length(); i++) {
            if (s.charAt(i)!=' ')
                q.append(s.charAt(i));
            else {
                stack[++top] = q.toString();
                q = new StringBuffer();
                continue;
            }
        }
        stack[++top] = q.toString();
    }

    /*判空*/
    Boolean isempty()
    {
        if(top==-1)
            return true;
        else
            return false;
    }

    /*取栈元素*/
    String peek(int i)
    {
        return stack[i];
    }

    /*栈大小*/
    int size()
    {
        return top+1;
    }

    public boolean isNumeric(int n){
            for (int i = stack[n].length();--i>=0;){
                    if (!Character.isDigit(stack[n].charAt(i))){
                        if (stack[n].charAt(i)!='-')
                            return false;
                        }
                }
            return true;
    }

    public boolean isRational(int n){
        for (int i = stack[n].length();--i>=0;){
            if (!Character.isDigit(stack[n].charAt(i))){
                if (stack[n].charAt(i)=='/')
                    return true;
            }
        }
        return false;
    }

    public void move(int i){
        top = top - 2;
        for (int j = i;j<=top;j++){
            stack[j] = stack[j+2];
        }
    }


    public void put(String s,int i){
        stack[i] = s;
    }
}
