import java.util.Random;

public class Teacher {
    StringBuffer que = new StringBuffer();
    Random rand = new Random();
    int N;
    public void CreStr(int n){
        int c;
        N = 10;
        c = rand.nextInt(100)%4;
        que = new StringBuffer();
        for (int i = 0;i<n;i++){
            switch (c){
                case 0:
                    que.append('+');
                    break;
                case 1:
                    que.append('-');
                    break;
                case 2:
                    que.append('*');
                    break;
                case 3:
                    que.append('/');
                    break;
            }
            c = rand.nextInt(4);
        }
    }
    public void InpNum(){
        int num;
        int i;
        StringBuffer temp = new StringBuffer();
        num = rand.nextInt(N) + 1;
        temp.append(Integer.toString(num));
        for (i = 0;i<que.length();i++){
                num = rand.nextInt(N) + 1;
                temp.append(que.charAt(i));
                temp.append(Integer.toString(num));
        }
        que = temp;
    }
    public void InPar(){
        StringBuffer temp = new StringBuffer();
        int i,j;
        int flag;
        int f = 3;
        StringBuffer s = new StringBuffer();
        for (i = 0;i<que.length();){
            if (Character.isDigit(que.charAt(i))){
                while (Character.isDigit(que.charAt(i))){
                    temp.append(que.charAt(i++));
                    if (que.length()==i) {
                        if (f==0) {
                            temp.append(')');
                            f = 3;
                        }
                        break;
                    }
                }
                if (f==0) {
                    flag = rand.nextInt(2);
                    if (flag==1) {
                        temp.append(')');
                        f = 3;
                    }
                }
                continue;
            }
            else {
                flag = rand.nextInt(100)%2;
                if (flag==1&&f==3){
                    s.append(que.charAt(i));
                    j = i + 1;
                    s.append('(');
                    while (j<que.length()&&f!=0) {
                        if (Character.isDigit(que.charAt(j))) {
                            while (Character.isDigit(que.charAt(j))) {
                                s.append(que.charAt(j++));
                                if (que.length() == j) {
                                    f = 0;
                                    break;
                                }
                            }
                            f--;
                        } else {
                            s.append(que.charAt(j++));
                            f--;
                        }
                    }
                    temp.append(s);
                    s = new StringBuffer();
                    i = j;
                }
                else {
                        temp.append(que.charAt(i++));
                    }
                }
            }
        if (f!=3) {
            temp.append(')');
        }
        que = temp;
    }
    public void DelPar() throws Exception {
        StringBuffer temp = new StringBuffer();
        StringBuffer s = new StringBuffer();
        StringBuffer q = new StringBuffer();
        Calculator cq = new Calculator();
        Calculator ct = new Calculator();
        Transform t = new Transform();
        int i,j;
        int p = 0,N = 0;
        for (i=0;i<que.length();i++) {
            if (que.charAt(i)=='(') {
                N++;
            }
        }
        int [] flag = new int [N];
        for (p = 0;p<N;p++){
            flag[p] = 1;
        }
        p = 0;
        for (i=0;i<que.length();i++) {
            if (que.charAt(i)=='('){
                if (flag[p]==1) {
                    temp.append(s);
                    s = new StringBuffer();
                    j = i+1;
                    while (que.charAt(j)!=')'){
                        q.append(que.charAt(j));
                        j++;
                    }
                    j++;
                    temp.append(q);
                    q = new StringBuffer();
                    while (j<que.length()){
                        temp.append(que.charAt(j++));
                    }
                    ct.q = new Mystack(t.tran(temp));
                    cq.q = new Mystack(t.tran(que));
                    ct.jisuan();
                    cq.jisuan();
                    if (ct.result.equals(cq.result)){
                        que = temp;
                        DelPar();
                        return;
                    }
                    else{
                        flag[p] = 0;
                        p = 0;
                        i = -1;
                        temp = new StringBuffer();
                    }
                }
                else {
                    s.append('(');
                    p++;
                }
            }
            else {
                s.append(que.charAt(i));
            }
            if (p==N) {
                return;
            }
        }

    }
    public void Space() throws Exception {
        int i;
        StringBuffer temp = new StringBuffer();
        for (i = 0; i < que.length();) {
            if (Character.isDigit(que.charAt(i))) {
                while (Character.isDigit(que.charAt(i))) {
                    temp.append(que.charAt(i++));
                    if (que.length() == i)
                        break;
                }
                temp.append(' ');
            }
            else {
                temp.append(que.charAt(i++));
                temp.append(' ');
            }
        }
            temp.deleteCharAt(temp.length() - 1);
            que = temp;
        }

    public void CreQue(int n) throws Exception{
        CreStr(n);
        InpNum();
        InPar();
        DelPar();
        //Space();
    }
}
