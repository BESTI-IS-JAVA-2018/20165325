import junit.framework.TestCase;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.*;

public class TestStudent extends TestCase {
    public static void main(String[] args) {
        List<Student> list = new LinkedList<Student>();
        list.add(new Student("20165323", "杨金川",580.0));
        list.add(new Student("20165324", "何春江",640.0));
        list.add(new Student("20165325", "李东骏",620.0));
        list.add(new Student("20165326", "陈卓",600.0));
        list.add(new Student("20165327", "杨靖涛",570.0));
        Iterator<Student> iter = list.iterator();
        System.out.println("排序前,链表中的数据");
        Collections.shuffle(list);
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName()+ " "+stu.getTotalScore());
        }
        Collections.sort(list,new scoreComparator());
        System.out.println("排序后,链表中的数据");
        iter = list.iterator();
        while (iter.hasNext()) {
            Student stu = iter.next();
            System.out.println(stu.getId() + " " + stu.getName()+ " "+stu.getTotalScore());
        }
    }
    /*@Test
    public void testID(){
        assertEquals();
    }*/

}
