import java.util.Iterator;

public class MyList {
    public static void main(String[] args) {
        //选用合适的构造方法，用你学号前后各两名同学的学号创建四个结点
        Node<Integer> S1 = new Node<Integer>(20165323, null);
        Node<Integer> S2 = new Node<Integer>(20165324, null);
        Node<Integer> S3 = new Node<Integer>(20165326, null);
        Node<Integer> S4 = new Node<Integer>(20165327, null);
        //把上面四个节点连成一个没有头结点的单链表
        S1.next = S2;
        S2.next = S3;
        S3.next = S4;
        //遍历单链表，打印每个结点的
        Node<Integer> s = S1;
        while (s != null) {
            System.out.println(s.data);
            s = s.next;
        }
        System.out.println();
        //把你自己插入到合适的位置（学号升序）
        Node<Integer> M = new Node<Integer>(20165325, null);
        s = S1;
        while (s != null) {
            if (s.data < 20165325 && s.next.data > 20165325) {
                M.next = s.next;
                s.next = M;
                break;
            }
            else {
                s = s.next;
            }
        }
        System.out.println();
        //遍历单链表，打印每个结点的
        s = S1;
        while (s != null) {
            System.out.println(s.data);
            s = s.next;
        }
        System.out.println();
        //从链表中删除自己
        s = S1;
        while (s != null) {
            if (s.next.data == 20165325) {
                s.next = s.next.next;
                break;
            }
            else {
                s = s.next;
            }
        }
        System.out.println();
        //遍历单链表，打印每个结点的
        s = S1;
        while (s != null) {
            System.out.println(s.data);
            s = s.next;
        }
    }
}
